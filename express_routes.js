// Import necessary modules
const express = require('express');
const app = express();

// 1. Use routers to clean up the code.
const mainRouter = express.Router();
app.use('/', mainRouter);

// 2. If you are facing issues with file paths, ask the instructor.

// 3. Add a page not found for cases where the route doesn't exist, return a status code 404.
mainRouter.use((req, res, next) => {
  res.status(404).send('Page not found');
});

// 4. Now make /admin routes as a separate filter.
const adminRouter = express.Router();
app.use('/admin', adminRouter);

// 5. Now make a /shop route for all the routes that call the shop.js file.
const shopRouter = require('./routes/shop'); // Assuming shop.js exists in the 'routes' folder
app.use('/shop', shopRouter);

// Example /admin routes
adminRouter.get('/dashboard', (req, res) => {
  res.send('Admin Dashboard');
});

// Example /shop routes (assuming shop.js handles these)
// routes/shop.js might look like:
// const express = require('express');
// const router = express.Router();
// router.get('/products', (req, res) => { /* logic for handling /shop/products */ });
// Add more routes as needed...

// Start the server
const PORT = 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
