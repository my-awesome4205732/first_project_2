const express = require('express');
const path = require('path');

const app = express();
const port = 3000;

// Serve static files from the public directory
app.use(express.static(path.join(__dirname, 'public')));

// Use views from the views directory
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs'); // Change 'html' to 'ejs'

// Routes
app.get('/', (req, res) => {
  res.render('index');
});

app.get('/contactus', (req, res) => {
  res.render('contactus');
});

app.post('/success', (req, res) => {
  // Process form submission logic here
  res.render('success');
});

// 404 Not Found
app.use((req, res) => {
  res.status(404).render('notfound');
});

// Start the server
app.listen(port, () => {
  console.log(`Server is running at http://localhost:${port}`);
});
