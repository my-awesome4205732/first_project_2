const userForm = document.getElementById("userForm");
const enteredDetailsList = document.getElementById("enteredDetails");

userForm.addEventListener("submit", async (e) => {
    e.preventDefault();

    const name = document.getElementById("name").value;
    const email = document.getElementById("email").value;
    const age = document.getElementById("age").value;

    const enteredDetails = { name, email, age };

    try {
        // Simulate a POST request to add entered details
        const response = await axios.post("https://crudcrud.com/api/f74b86a9a6ac4258ab354e08af733841/k", enteredDetails);

        // Display the entered details on the browser
        displayEnteredDetails(enteredDetails);

        userForm.reset();
    } catch (error) {
        console.error("Error adding entered details:", error);
    }
});

function displayEnteredDetails(details) {
    const listItem = document.createElement("li");
    listItem.textContent = `Name: ${details.name}, Email: ${details.email}, Age: ${details.age}`;
    enteredDetailsList.appendChild(listItem);
}
