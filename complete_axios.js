const userForm = document.getElementById("userForm");
const enteredDetailsList = document.getElementById("enteredDetails");

let enteredDetails = [];

userForm.addEventListener("submit", async (e) => {
    e.preventDefault();

    const name = document.getElementById("name").value;
    const email = document.getElementById("email").value;
    const age = document.getElementById("age").value;

    const newDetail = { name, email, age };

    try {
        // Simulate a POST request to add entered details
        const response = await axios.post("https://crudcrud.com/api/2d4e25673a1e4e799ca601dc8cbab4de/a", newDetail);

        newDetail.id = response.data.id; // Assign an ID to the new detail
        enteredDetails.push(newDetail);

        // Display the entered details on the browser
        displayEnteredDetails();

        userForm.reset();
    } catch (error) {
        console.error("Error adding entered details:", error);
    }
});

function displayEnteredDetails() {
    enteredDetailsList.innerHTML = "";

    enteredDetails.forEach((detail) => {
        const listItem = document.createElement("li");
        listItem.innerHTML = `<strong>Name:</strong> ${detail.name}, <strong>Email:</strong> ${detail.email}, <strong>Age:</strong> ${detail.age} 
                            <button onclick="editDetail(${detail.id})">Edit</button>
                            <button onclick="deleteDetail(${detail.id})">Delete</button>`;
        enteredDetailsList.appendChild(listItem);
    });
}

// ... (existing code above)

async function editDetail(detailId) {
    const detailIndex = enteredDetails.findIndex(detail => detail.id === detailId);
    if (detailIndex === -1) return;

    const editedName = prompt("Enter new name:", enteredDetails[detailIndex].name);
    const editedEmail = prompt("Enter new email:", enteredDetails[detailIndex].email);
    const editedAge = prompt("Enter new age:", enteredDetails[detailIndex].age);

    if (editedName && editedEmail && editedAge) {
        try {
            // Simulate a PUT request to update the detail
            const updatedDetail = {
                name: editedName,
                email: editedEmail,
                age: editedAge
            };

            await axios.put(`https://crudcrud.com/api/2d4e25673a1e4e799ca601dc8cbab4de/a/${detailId}`, updatedDetail);

            enteredDetails[detailIndex] = {
                id: detailId,
                ...updatedDetail
            };

            displayEnteredDetails();
        } catch (error) {
            console.error("Error updating detail:", error);
        }
    }
}


async function deleteDetail(detailId) {
    try {
        // Use the correct URL to perform the DELETE request
        axios.delete(`https://yourbackendapi.com/delete-endpoint/${detailId}`, {
            withCredentials: true,
        });
        
       
        // Remove the deleted detail from the enteredDetails array
        enteredDetails = enteredDetails.filter(detail => detail.id !== detailId);

        displayEnteredDetails();
    } catch (error) {
        console.error("Error deleting detail:", error);
    }
}

// Initial data fetch
async function fetchEnteredDetails() {
    try {
        const response = await axios.get("https://crudcrud.com/api/2d4e25673a1e4e799ca601dc8cbab4de/a");
        enteredDetails = response.data;
        displayEnteredDetails();
    } catch (error) {
        console.error("Error fetching entered details:", error);
    }
}

fetchEnteredDetails();
