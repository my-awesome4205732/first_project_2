const express = require('express');
const bodyParser = require('body-parser');

const app = express();
app.use(bodyParser.urlencoded({ extended: false }));

app.get('/add-product', (req, res) => {
  res.send('<form action="/product" method="POST"><input type="text" name="productName"><input type="text" name="productSize"><button type="submit">Add Product</button></form>');
});

app.post('/product', (req, res) => {
  console.log(req.body.productName);
  console.log(req.body.productSize);
  res.redirect('/');
});

app.listen(3000);