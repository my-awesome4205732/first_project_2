// Assuming you have a server-side script (e.g., Node.js with Express) to handle messages.
// You need to implement the sendMessageToServer function.

// Example (server.js):

const express = require('express');
const fs = require('fs');
const app = express();
const port = 3000;

app.use(express.json());

app.post('/sendMessage', (req, res) => {
  const { username, message } = req.body;

  // Store the message in a file
  const chatData = { username: username, message: message };
  fs.appendFileSync('chatData.json', JSON.stringify(chatData) + '\n');

  res.send('Message sent successfully!');
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
// Assuming you have a function to read messages from the file and update the chat display.

// Example (server.js continued):

// Assuming you have a function readMessagesFromFile defined elsewhere.

// Example (server.js continued):

app.get('/getMessages', (req, res) => {
    const messages = readMessagesFromFile();
  
    // Send the messages back to the client (you may use res.json here to send JSON data).
    res.send(messages);
  });
  
  // Function to read messages from the file
  function readMessagesFromFile() {
    const rawData = fs.readFileSync('chatData.json', 'utf8');
    const messages = rawData.split('\n').filter(line => line.trim() !== '').map(line => JSON.parse(line));
    return messages;
  }
  