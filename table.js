// table.js

const orderForm = document.getElementById("orderForm");
const orderList = document.getElementById("orderList");
const tableSelect = document.getElementById("table");
const selectedTableHeading = document.getElementById("selectedTableHeading");

orderForm.addEventListener("submit", async (e) => {
    e.preventDefault();

    const item = document.getElementById("item").value;
    const quantity = document.getElementById("quantity").value;
    const table = tableSelect.value;

    const newOrder = {
        item: item,
        quantity: quantity
    };

    try {
        // Simulate a POST request to add the order to the selected table
        await axios.post(`https://crudcrud.com/api/ec24ea89bada4ef7b2a6dc37f8675664/app${table}`, newOrder);

        // Clear form fields
        orderForm.reset();

        // Update the order list and selected table heading
        fetchOrders(table);
        selectedTableHeading.textContent = `Selected Table: ${table}`;
    } catch (error) {
        console.error("Error adding order:", error);
    }
});

async function fetchOrders(table) {
    try {
        // Simulate a GET request to fetch orders from the selected table
        const response = await axios.get(`https://crudcrud.com/api/ec24ea89bada4ef7b2a6dc37f8675664/app${table}`);

        // Display the orders
        orderList.innerHTML = "";
        response.data.forEach(order => {
            const orderItem = document.createElement("div");
            orderItem.innerHTML = `
                <p><strong>Item:</strong> ${order.item}</p>
                <p><strong>Quantity:</strong> ${order.quantity}</p>
                <button onclick="deleteOrder('${table}', '${order._id}')">Delete</button>
            `;
            orderList.appendChild(orderItem);
        });
    } catch (error) {
        console.error("Error fetching orders:", error);
    }
}

async function deleteOrder(table, orderId) {
    try {
        // Simulate a DELETE request to delete the order from the selected table
        const response = await axios.delete(`https://crudcrud.com/api/ec24ea89bada4ef7b2a6dc37f8675664/app${table}/${orderId}`);

        // Update the order list
        fetchOrders(table);
    } catch (error) {
        console.error("Error deleting order:", error);
    }
}

// Initial data fetch
document.addEventListener("DOMContentLoaded", () => {
    const selectedTable = tableSelect.value;
    fetchOrders(selectedTable);
});
