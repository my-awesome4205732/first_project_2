const inputForm = document.getElementById("inputForm");
const outputDiv = document.getElementById("output");

inputForm.addEventListener("submit", function (e) {
    e.preventDefault();

    const input1Value = document.getElementById("input1").value;
    const input2Value = document.getElementById("input2").value;
    const input3Value = document.getElementById("input3").value;

    const item = { input1: input1Value, input2: input2Value, input3: input3Value };

    // Use Axios to post the data to the server
    axios.post('https://crudcrud.com/api/b576d1159069489da90c5d48fede1c2f/mike', item)
        .then(response => {
            console.log("Data posted successfully:", response.data);
            updateOutput(item);
            inputForm.reset();
        })
        .catch(error => {
            console.error("Error posting data:", error);
        });
});

function updateOutput(newItem) {
    const itemDiv = document.createElement("div");
    itemDiv.className = "output-item";

    const contentDiv = document.createElement("div");
    contentDiv.innerHTML = `
        <p><strong>Input 1:</strong> ${newItem.input1}</p>
        <p><strong>Input 2:</strong> ${newItem.input2}</p>
        <p><strong>Input 3:</strong> ${newItem.input3}</p>
    `;

    const buttonDiv = document.createElement("div");
    buttonDiv.innerHTML = `
        <button onclick="editItem()">Edit</button>
        <button onclick="deleteItem()">Delete</button>
    `;

    itemDiv.appendChild(contentDiv);
    itemDiv.appendChild(buttonDiv);

    outputDiv.appendChild(itemDiv);
}
function editItem(index) {
    const items = JSON.parse(localStorage.getItem("items")) || [];
    const editedItem = items[index];
    if (editedItem) {
        document.getElementById("input1").value = editedItem.input1;
        document.getElementById("input2").value = editedItem.input2;
        document.getElementById("input3").value = editedItem.input3;

        items.splice(index, 1);
        localStorage.setItem("items", JSON.stringify(items));
        updateOutput(editedItem);
    }
}

function deleteItem(index) {
    const items = JSON.parse(localStorage.getItem("items")) || [];
    const deletedItem = items[index];
    if (deletedItem) {
        items.splice(index, 1);
        localStorage.setItem("items", JSON.stringify(items));
        outputDiv.removeChild(outputDiv.childNodes[index]);
    }
}

// Initial update of output on page load
updateOutput();

// Modify the editItem and deleteItem functions as needed
// ...

// Initial update of output on page load
// ...
